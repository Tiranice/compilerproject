
%debug
%verbose    /*generate file: tiger.output to check grammar*/
%{
/*

PROGRAMMER: L. K. Silkeutsabay
PROGRAM #: 3
DUE DATE: Wednesday, 9302020
INSTRUCTOR: Dr. Zhijiang Dong

*/
#include <iostream>
#include <string>
#include "ErrorMsg.h"
#include <FlexLexer.h>

using std::string;
using std::stringstream;



int yylex(void);        /* function prototype */
void yyerror(char *s);  //called by the parser whenever an eror occurs

%}

%union {
    int     ival;   //integer value of INT token
    std::string* sval;  //pointer to name of IDENTIFIER or value of STRING
                    //I have to use pointers since C++ does not support
                    //string object as the union member
}

/* TOKENs and their associated data type */
%token <sval> ID STRING
%token <ival> INT

%token
  COMMA "," COLON ":" SEMICOLON ";" LPAREN "(" RPAREN ")" LBRACK "[" RBRACK "]"
  LBRACE "{" RBRACE "}" DOT "."
  ARRAY "array" IF "if" THEN "then" ELSE "else" WHILE "while" FOR "for" TO "to" 
  DO "do" LET "let" IN "in" END "end" OF "of"
  BREAK "break" NIL "nil"
  FUNCTION "function" VAR "var" TYPE "type"
  ASSIGN ":=" PLUS "+" MINUS "-" TIMES "*" DIVIDE "/" EQ "=" NEQ "!=" LT "<" LE "<="
  GT ">" GE ">=" AND "&" OR "|"

/* add your own predence level of operators here */
%nonassoc ":="
%left "|"
%left "&"
%nonassoc "=" "!=" ">" "<" ">=" "<="
%left "-" "+"
%left "*" "/"
%left UMINUS
%right "(" "[" "{" "."

%nonassoc "then"
%nonassoc "else"
%nonassoc "do" "of"
%nonassoc "to"
%nonassoc "for" "while"

%start program

%%

/* This is a skeleton grammar file, meant to illustrate what kind of
 * declarations are necessary above the %% mark.  Students are expected
 *  to replace the two dummy productions below with an actual grammar.
 */

program : expr

expr:   STRING
|       INT
|       NIL
|       ID
|       lvalue_not_id
|       "-" expr %prec UMINUS
|       binary_op
|       lvalue ":=" expr
|       ID "(" explist ")"
|       "(" expseq ")"
|       ID "{" fieldlist "}"
|       array_create
|       if_stmt
|       "while" expr "do" expr
|       "for" ID ":=" expr "to" expr "do" expr
|       "break"
|       "let" decl_list "in" expseq "end"
;

binary_op:  expr "+" expr
|           expr "-" expr %prec "+"
|           expr "*" expr
|           expr "/" expr
|           expr "=" expr
|           expr "!=" expr
|           expr "<" expr
|           expr ">" expr
|           expr "<=" expr
|           expr ">=" expr
|           expr "&" expr
|           expr "|" expr
;


if_stmt:    "if" expr "then" expr
|           "if" expr "then" expr "else" expr
;

explist:    expr
|           explist "," expr
;

expseq: /* empty */
|       expr
|       expseq ";" expr
|       error
|       expseq ";" error
;

fieldlist:  ID EQ expr
|           fieldlist "," ID EQ expr
;

lvalue: ID
|       lvalue_not_id
;

lvalue_not_id:  lvalue "." ID
|               ID "[" expr "]"
|               lvalue_not_id "[" expr "]"
;

array_create:   ID "[" expr "]" "of" expr
;

decl_list:  dec
|           decl_list dec
;

dec:    type_dec
|       variable_dec
;

type_dec:    "type" ID "=" type_name_def
;

type_name_def:  ID
|               "[" type_fields "]"
|               "array" "of" ID
;

type_fields: /* empty */
|           type_field
|           type_fields "," type_field
;

type_field:  ID ":" ID
;

variable_dec:   "var" ID ":=" expr
|               "var" ID ":" ID ":=" expr
;


%%
extern yyFlexLexer  lexer;
int yylex(void)
{
    return lexer.yylex();
}

void yyerror(char *s)
{
    extern int  linenum;            //line no of current matched token
    extern int  colnum;
    extern void error(int, int, std::string);

    error(linenum, colnum, s);
}

